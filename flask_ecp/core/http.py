# -*- coding: utf-8 -*-
"""
Flask-ECP http module
"""
from flask import request, jsonify, make_response
from functools import wraps

import requests
import json
import logging


class Http(object):
    """"""

    def __init__(self):
        """Init object"""
        # logger for secure manager
        self.logger = logging.getLogger('app.flask_ecp.http')

    def make_request(self, url, method, payload):
        """"""
        try:
            # set proper content-type header
            headers = {
                'content-type': 'application/json'
            }

            if method == 'GET':
                req = requests.get(url, data=json.dumps(payload), headers=headers)

            elif method == 'POST':
                req = requests.post(url, data=json.dumps(payload), headers=headers)

            else:
                raise ValueError('Method not allowed')

            if req.status_code == 200:
                return True, req.json()
            else:
                return False, req.json()
        except Exception, e:
            self.logger.error(str(e), exc_info=True)
            return False, {
                'status': 'error',
                'error_type': str(type(e)),
                'error_msg': repr(e),
                'module_name': str(self.__module__),
                'class_name': str(self.__class__),
                'func_name': 'make_request'
            }

    @staticmethod
    def create_response(content, code, content_type='application/json; charset="UTF-8"', cors=None):
        """Create response object

        Args:
            content (str(example: dump json)): content of the response
            code (int): http code of the response
            content_type='application/json; charset="UTF-8"' (str): content type of the response
            cors (str): cors header
        Returns:
            Flask response object

        """

        resp = make_response(content, code)
        if cors is not None:
            resp.headers['Access-Control-Allow-Origin'] = cors
        resp.headers['Content-Type'] = content_type
        resp.headers['Cache-Control'] = 'no-cache'
        return resp
