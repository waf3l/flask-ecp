# -*- coding: utf-8 -*-
"""
Encryption module
"""

from Crypto.Cipher import AES
from Crypto.Hash import SHA256
from Crypto.Hash import MD5

import logging
import base64


class Encryption(object):
    """Object for encryption / decryption purpose """

    def __init__(self, cipher_key, iv_key):
        """Init object"""
        # logger for secure manager
        self.logger = logging.getLogger('app.flask_ecp.encryption')

        self.cipher_hash_key = self.hash_key(cipher_key, 32)
        self.iv_hash_key = self.hash_key(iv_key, 16)
        if len(self.iv_hash_key) != 16:
            raise ValueError('IV is not 16 bits long, length: %s' % str(len(self.iv_hash_key)))

    @staticmethod
    def hash_key(key, length=32):
        """
        Generate hash from key value

        Args:
            key (string): input value to be converted into string of hash object

        Returns:
            String of hash object
        """

        # digest of hash key
        if length == 32:
            # create hash algorithm
            hash_alg = SHA256.new(key)

            return hash_alg.digest()
        elif length == 16:
            hash_alg = MD5.new(key)

            return hash_alg.digest()
        else:
            raise ValueError('Arg length must be 16 or 32 integer')

    @staticmethod
    def create_cipher(cipher_key, iv_key):
        """
        Create cipher object

        Args:
            key (str): SHA256 hash digest string (needed 32 bytes long key)
            iv (str): 16 bytes long initialization vector
        Returns:
            cipher object
        """

        # create new AES cipher
        return AES.new(cipher_key, AES.MODE_CFB, iv_key)

    def encrypt(self, payload, cipher_key=None, iv_key=None):
        """
        Encrypt data with specified secure key

        Args:
            key (str): secure key for encrypt data
            payload (text): text data to encrypt
        Returns:
            Tuple of (Boolean,{iv,encrypt_data}),
            if boolean is False the the return data is error dict data
        """
        try:
            # create cipher object
            cipher = self.create_cipher(
                self.cipher_hash_key if cipher_key is None else cipher_key,
                self.iv_hash_key if iv_key is None else iv_key
            )

            return True, {
                'encrypted_data': base64.b64encode(cipher.encrypt(payload))
            }

        except Exception, e:
            self.logger.error(str(e), exc_info=True)
            return False, {
                'code': 500,
                'error_type': str(type(e)),
                'error_msg': repr(e),
                'module_name': str(self.__module__),
                'class_name': str(self.__class__),
                'func_name': 'encrypt'
            }

    def decrypt(self, encrypted_data, cipher_key=None, iv_key=None):
        """
        Decrypt data with specified secure key

        Args:
            key (str): secure key for decrypt data
            encrypted_data (text): base64 encrypted data to decrypt
        Returns:
            Tuple of (Boolean, decrypted data),
            if boolean is False the the return data is error dict data
        """
        try:
            # create cipher object
            cipher = self.create_cipher(
                self.cipher_hash_key if cipher_key is None else cipher_key,
                self.iv_hash_key if iv_key is None else iv_key
            )

            # decrypt encrypted data
            decrypt_data = cipher.decrypt(base64.b64decode(encrypted_data))

            # return
            return True, decrypt_data

        except Exception, e:
            self.logger.error(str(e), exc_info=True)
            return False, {
                'code': 500,
                'error_type': str(type(e)),
                'error_msg': repr(e),
                'module_name': str(self.__module__),
                'class_name': str(self.__class__),
                'func_name': 'decrypt'
            }
