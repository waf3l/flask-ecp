# -*- coding: utf-8 -*-
"""
Authorization module
"""
from flask import current_app
from encryption import Encryption

import logging
import redis
import random
import string
import time


class Authorization(object):
    """Authorization"""

    def __init__(self):
        """Init object"""
        # logger for secure manager
        self.logger = logging.getLogger('app.flask_ecp.auth')

        # local cipher object
        self.enc = Encryption(
            current_app.config['FLASK_ECP']['cipher_key'],
            current_app.config['FLASK_ECP']['iv_key']
        )
        self.access_key = current_app.config['FLASK_ECP']['access_key']
        self.r_server = redis.Redis(
            host=current_app.config['REDIS']['host'],
            port=current_app.config['REDIS']['port'],
            password=current_app.config['REDIS']['password']
        )

    @staticmethod
    def generate_token(length=32):
        """Generate 32 characters length random string

        Args:
            length=32 (int): length of token to be generated

        Returns:
            Str with token data
        """
        return ''.join([random.choice(string.ascii_letters + string.digits) for n in xrange(length)])

    def add_token(self, token, ex=None):
        """Add token to redis

        Args:
            token (str): token key
            ex (int): expire time in milliseconds

        Returns:
            Redis replay
        """
        # generate timestamp
        timestamp = int(time.time())

        # add token to redis
        return self.r_server.set(token, timestamp, ex=ex)

    def get_token(self):
        """Generate new token"""
        # generate token
        token = self.generate_token()

        if self.add_token(token):
            # return encrypted token data
            return self.enc.encrypt(token)
        else:
            return False, {
                'code': 500,
                'error_type': 'RedisError',
                'error_msg': 'Can not save value at key',
                'module_name': str(self.__module__),
                'class_name': str(self.__class__),
                'func_name': 'get_token'
            }

    def validate_token(self, token):
        """"""
        token_test = self.r_server.get(token)
        if token_test is not None:
            self.r_server.delete(token)
            return True, {}
        else:
            return False, {
                'code': 401,
                'error_typ': 'BadToken',
                'error_msg': 'token is invalid'
            }

    def payload_authorization(self, access_key, cipher_key, iv_key):
        """"""

        return self.enc.encrypt(
            access_key,
            self.enc.hash_key(cipher_key, 32),
            self.enc.hash_key(iv_key, 16)
        )

    def check_authorization(self, encrypted_auth_data):
        """"""
        try:
            status_decrypt, response_decrypt = self.enc.decrypt(
                encrypted_auth_data['encrypted_data']
            )
            if status_decrypt:
                if self.access_key == response_decrypt:
                    return self.get_token()
                else:
                    return False, {
                        'code': 500,
                        'error_type': 'AuthorizeError',
                        'error_msg': 'Invalid authorization data',
                        'module_name': str(self.__module__),
                        'class_name': str(self.__class__),
                        'func_name': 'check_authorization'
                    }
            else:
                return status_decrypt, response_decrypt
        except Exception, e:
            self.logger.error(str(e), exc_info=True)
            return False, {
                'code': 500,
                'error_type': str(type(e)),
                'error_msg': repr(e),
                'module_name': str(self.__module__),
                'class_name': str(self.__class__),
                'func_name': 'check_authorization'
            }

    def check_token(self, token_encrypted_data):
        """"""
        try:
            status_decrypt_token, token_decrypt = self.enc.decrypt(
                token_encrypted_data['encrypted_data']
            )
            if status_decrypt_token:
                return self.validate_token(token_decrypt)
            else:
                return status_decrypt_token, token_decrypt
        except Exception, e:
            self.logger.error(str(e), exc_info=True)
            return False, {
                'code': 500,
                'error_type': str(type(e)),
                'error_msg': repr(e),
                'module_name': str(self.__module__),
                'class_name': str(self.__class__),
                'func_name': 'check_authorization'
            }
