# -*- coding: utf-8 -*-
"""
Module for encrypt and decrypt data
"""
from flask import current_app
from simplecrypt import encrypt, decrypt

import base64
import logging
import os


class Secure(object):
    """Main class"""

    def __init__(self, root_logger='app', passwd=None):
        """Init object"""

        # logger
        self.logger = logging.getLogger('{0}.secure'.format(root_logger))
        self.logger.info('init ecp secure module')

        # passwd
        if passwd is not None:
            self.passwd = base64.b64encode(passwd)
        else:
            if current_app.config.get('FLASK_ECP', None) is not None:
                if current_app.config['FLASK_ECP'].get('passwd', None) is not None:
                    self.passwd = base64.b64encode(current_app.config['FLASK_ECP']['passwd'])
                else:
                    FLASK_ECP_PASSWD = os.getenv('FLASK_ECP_PASSWD', None)

                    if FLASK_ECP_PASSWD is not None:
                        self.passwd = base64.b64encode(FLASK_ECP_PASSWD)
                    else:
                        raise TypeError(
                            'Can not find FLASK_ECP.passwd key in app config and password is not given on init object'
                        )
            else:
                raise TypeError('Can not find FLASK_ECP key in app config and password is not given on init object')

    def encrypt_data(self, data):
        if data is None:
            raise TypeError('data value can not be None')

        return base64.b64encode(encrypt(base64.b64decode(self.passwd), data))

    def decrypt_data(self, encrypted_data):
        if encrypted_data is None:
            raise TypeError('encrypted_data value can not be None')

        return decrypt(base64.b64decode(self.passwd), base64.b64decode(encrypted_data))
