# -*- coding: utf-8 -*-
"""
Flask extension for secure communication between server and client
"""
from flask import request, jsonify
from functools import wraps

from flask_ecp.core.authorize import Authorization
from flask_ecp.core.http import Http
from flask_ecp.core.secure import Secure

import logging
import json


class ECP(object):
    """Encrypted communication protocol"""

    def __init__(self, app=None):
        """Init object"""

        # logger
        self.root_logger_name = 'app'
        self.module_logger_name = 'flask_ecp'
        self.full_logger_name = '{0}.{1}'.format(self.root_logger_name, self.module_logger_name)
        self.logger = logging.getLogger(self.full_logger_name)
        self.logger.info('Flask ECP logger init')

        # local app object
        self.app = None

        # authorization object
        self.auth = None

        # http object
        self.http = None

        # secure object
        self.sec = None

        if app is not None:
            self.init_app(app)

    def init_app(self, app):
        """Flask application factor to create instance of this object

        Args:
            app (Flask app): flask app object.

        """
        # local flask app object
        self.app = app

        with app.app_context():
            # init auth object
            self.auth = Authorization()

            # init http object
            self.http = Http()

            # init secure object
            self.sec = Secure(root_logger=self.full_logger_name)

    def request_token(self, url, access_key, cipher_key, iv_key):
        """
        Prepare data to authorize and get back token value

        Args:
            url (str): http address for call to authorize and get back token value.
            cipher_key (str): encryption key.
            iv_key (str): encryption initialize vector key.
            access_key (str): key for authorize user and generate token value

        Returns:
            Tuple (boolean, return data)
            If boolean true then return data is encrypted token value,
            else return data has error information
        """
        try:
            status_auth_payload, response_auth_payload = self.auth.payload_authorization(access_key, cipher_key, iv_key)
            if status_auth_payload:
                # make request and return token or validation error
                return self.http.make_request(url, 'GET', response_auth_payload)
            else:
                # return error data
                return status_auth_payload, response_auth_payload
        except Exception, e:
            self.logger.error(str(e), exc_info=True)
            return False, {
                'status': 'error',
                'error_type': str(type(e)),
                'error_msg': repr(e),
                'module_name': str(self.__module__),
                'class_name': str(self.__class__),
                'func_name': 'request_token'
            }

    def authorize(self, encrypted_auth_data):
        """Authorize access_key from encrypted data and return access token

        Get the encrypted authentication data, try to decrypt it,
        when decryption is success, then compare access_key from decrypted data
        with local saved access_key. If both access_key are equele then generate access token
        encrypt it and return encrypted data.

        Args:
            encrypted_auth_data (dict): encrypted authentication data

        Returns:
            Tuple (boolean, return data)
            If boolean true then return data is encrypted token value,
            else return data has error information


        """
        return self.auth.check_authorization(encrypted_auth_data)

    def token_require(self, f):
        """Decorator for url api call that checks if in the request data is token information.

        When token information is in the request data, that token if validated with local token db.
        If token is valid the url api call is passed, else the api call is bloken and return
        the error information to the client.

        Args:
            f (func): api url view func

        Returns:
            If token valid return wrapped func, else return dict with keys:
            - error_type: (KeyError, BadToken),
            - code: (HTTP Code: 401),
            - error_msg: error message

        .. warning::
        If token is localized in 'request.json' the decorator will break search the token
        information in request.values and will pass the api url view function

        """

        @wraps(f)
        def decorated_function(*args, **kwargs):

            if 'Origin' in request.headers:
                request_origin = request.headers['Origin']
            else:
                request_origin = None

            if hasattr(request, 'json'):
                # encrypted token
                token_encrypted_data = request.json.get('token', None)

                # encrypted data
                data = request.json.get('data', None)
                data_type = request.json.get('data_type', None)

                if token_encrypted_data is not None:
                    status_check_token, response_check_token = self.auth.check_token(token_encrypted_data)
                    if not status_check_token:
                        return self.http.create_response(jsonify(response_check_token), 401, cors=request_origin)
                else:
                    return self.http.create_response(jsonify(
                        {
                            'error_type': 'KeyError',
                            'code': 401,
                            'error_msg': 'can not find token key'
                        }
                    ), 401, cors=request_origin)

                if data is not None:
                    decrypt_status, decrypted_data = self.auth.enc.decrypt(data['encrypted_data'])
                    if decrypt_status:
                        if data_type == 'dict':
                            decrypted_data = json.loads(decrypted_data)
                        elif data_type == 'list':
                            decrypted_data = decrypted_data.split(';')
                        request.json['data'] = decrypted_data
                    else:
                        return self.http.create_response(jsonify(decrypted_data), 500, cors=request_origin)

            return f(*args, **kwargs)

        return decorated_function

    def make_api_call(self, url, method, token, data, cipher_key=None, iv_key=None):
        """Make api call to bot

        Method call bot api address with headers set to 'content-type':'application/json'.
        We send to bot json with two keys(token,data). Token key has token information for authentication, and
        data key has data information for bot. Return information is json.

        Args:
            url (str): url address to call
            method (str): HTTP METHOD to call bot endpoint api url
            token (str): token for bot authentication
            data (dict): dict object with data information for bot
        Returns:
            If success return tuple (True, return data) else return tuple (False, error data)
        """
        try:
            if isinstance(token, dict):
                encrypted_data = token.get('encrypted_data', None)
                if encrypted_data is None:
                    raise KeyError('Missing encrypted_data key')
                else:
                    # prepare payload data
                    data_type = None
                    if isinstance(data, dict):
                        data_type = 'dict'
                        status_encrypt, data_encrypt = self.auth.enc.encrypt(
                            json.dumps(data),
                            self.auth.enc.hash_key(cipher_key, 32),
                            self.auth.enc.hash_key(iv_key, 16)
                        )
                    elif isinstance(data, str):
                        data_type = 'str'
                        status_encrypt, data_encrypt = self.auth.enc.encrypt(
                            data,
                            self.auth.enc.hash_key(cipher_key, 32),
                            self.auth.enc.hash_key(iv_key, 16)
                        )

                    elif isinstance(data, list):
                        data_type = 'list'
                        status_encrypt, data_encrypt = self.auth.enc.encrypt(
                            ';'.join(data),
                            self.auth.enc.hash_key(cipher_key, 32),
                            self.auth.enc.hash_key(iv_key, 16)
                        )

                    elif data is None:
                        data_type = 'None'
                        status_encrypt = True
                        data_encrypt = None

                    else:
                        raise AttributeError('Unsupported data type')

                    if status_encrypt is not None:
                        payload = {
                            'token': token,
                            'data': data_encrypt,
                            'data_type': data_type
                        }
                        return self.http.make_request(url, method, payload)
                    else:
                        return status_encrypt, data_encrypt
            else:
                raise TypeError('Token value must be a dict')

        except Exception, e:
            self.logger.error(repr(e), exc_info=True)
            return False, {
                'status': 'error',
                'error_type': str(type(e)),
                'error_msg': repr(e),
                'module_name': str(self.__module__),
                'class_name': str(self.__class__),
                'func_name': 'make_api_call'
            }

    def content_type_json(self, f):
        """Check if request data is in json"""

        @wraps(f)
        def decorated_function(*args, **kwargs):
            """Check if request is application/json"""
            if 'Origin' in request.headers:
                request_origin = request.headers['Origin']
            else:
                request_origin = None

            if 'Content-Type' in request.headers:
                if request.headers['Content-Type'] != 'application/json':
                    return self.create_response(jsonify({
                        'code': 400,
                        'error_typ': 'content-type',
                        'error_msg': 'bad content-type'
                    }), 400, cors=request_origin)

                return f(*args, **kwargs)

            else:
                return self.create_response(jsonify({
                    'code': 400,
                    'error_typ': 'content-type',
                    'error_msg': 'missing content-type'
                }), 400, cors=request_origin)

        return decorated_function
