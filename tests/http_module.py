from flask import Flask, jsonify
from flask.wrappers import Response
import os
import sys
import unittest
import mock
import json

app_dir = os.path.dirname(os.getcwd())
sys.path.append(app_dir)

from flask_ecp.core.http import Http

# flask app
app = Flask(__name__)
app.config.from_object('tests.config.Config')

class TestHttp(unittest.TestCase):

    @classmethod
    def setUpClass(cls):
        pass

    @classmethod
    def tearDownClass(cls):
        pass

    def setUp(self):
        with app.app_context():
            self.http = Http()

    def tearDown(self):
        pass

    def test_init(self):
        self.assertTrue(isinstance(self.http, Http))

    @mock.patch('flask_ecp.core.http.requests.get')
    @mock.patch('flask_ecp.core.http.requests.post')
    def test_make_request(self, mock_requests_post, mock_requests_get):
        url = 'http://test.me'
        headers = {
            'content-type': 'application/json'
        }

        authorization_payload = {
            'encrypted_data': 'some encrypted data'
        }

        mock_requests_get.return_value.status_code = 200
        mock_requests_post.return_value.status_code = 200

        status_get, response_get = self.http.make_request(url, 'GET', authorization_payload)

        mock_requests_get.assert_called_once_with(url, data=json.dumps(authorization_payload), headers=headers)
        self.assertTrue(status_get)

        status_post, response_post = self.http.make_request(url, 'POST', authorization_payload)

        mock_requests_post.assert_called_once_with(url, data=json.dumps(authorization_payload), headers=headers)
        self.assertTrue(status_post)

    def test_create_response(self):
        with app.test_request_context('/test'):
            response = self.http.create_response(jsonify({'code': 200}), 200)

        self.assertIsInstance(response, Response)

if __name__ == '__main__':
    unittest.main()
