import sys
import unittest

from tests.suite import tests


def run():
    result = unittest.TextTestRunner(verbosity=2).run(tests())

    if not result.wasSuccessful():
        sys.exit(1)

if __name__ == '__main__':
    run()
