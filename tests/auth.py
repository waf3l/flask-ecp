from flask import Flask

import os
import sys
import unittest

app_dir = os.path.dirname(os.getcwd())
sys.path.append(app_dir)

from flask_ecp.core.authorize import Authorization
from flask_ecp.core.encryption import Encryption

# flask app
app = Flask(__name__)
app.config.from_object('tests.config.Config')


class TestAuthorize(unittest.TestCase):

    @classmethod
    def setUpClass(cls):
        pass

    @classmethod
    def tearDownClass(cls):
        pass

    def setUp(self):
        with app.app_context():
            self.auth = Authorization()

    def tearDown(self):
        pass

    def test_init(self):
        self.assertTrue(isinstance(self.auth.enc, Encryption))
        self.assertTrue(self.auth.access_key == app.config['FLASK_ECP']['access_key'])

    def test_generate_token(self):
        token = self.auth.generate_token()
        self.assertTrue(len(token) == 32)

    def test_add_token(self):
        token = '123'
        self.auth.add_token(token)
        self.auth.r_server.delete(token)

    def test_token(self):
        status, data = self.auth.get_token()
        self.assertTrue(status)
        self.assertTrue(isinstance(data, dict))
        self.assertIsNotNone(data.get('encrypted_data', None))

    def test_payload_authorization(self):
        iv_key = self.auth.enc.hash_key('some iv', 16)
        cipher_key = self.auth.enc.hash_key('some access', 32)
        access_key = 'some access_key'
        status, data = self.auth.payload_authorization(access_key, cipher_key, iv_key)
        self.assertTrue(status)
        self.assertTrue(isinstance(data, dict))
        self.assertIsNotNone(data.get('encrypted_data', None))

    def test_check_authorization(self):
        iv_key = app.config['FLASK_ECP']['iv_key']
        cipher_key = app.config['FLASK_ECP']['cipher_key']
        access_key = app.config['FLASK_ECP']['access_key']

        status, data = self.auth.payload_authorization(access_key, cipher_key, iv_key)

        self.assertTrue(status)
        self.assertTrue(isinstance(data, dict))
        self.assertIsNotNone(data.get('encrypted_data', None))

        status_auth, data_auth = self.auth.check_authorization(data)
        self.assertTrue(status_auth)
        self.assertTrue(isinstance(data_auth, dict))
        self.assertIsNotNone(data_auth.get('encrypted_data', None))

if __name__ == '__main__':
    unittest.main()
