from enc import TestEncryption
from auth import TestAuthorize
from http_module import TestHttp

import unittest

def tests():
    suite = unittest.TestSuite()
    suite.addTest(unittest.makeSuite(TestEncryption))
    suite.addTest(unittest.makeSuite(TestAuthorize))
    suite.addTest(unittest.makeSuite(TestHttp))
    return suite
