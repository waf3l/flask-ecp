from flask import Flask

from Crypto.Random import get_random_bytes
from Crypto.Cipher import AES

import os
import sys
import unittest

app_dir = os.path.dirname(os.getcwd())
sys.path.append(app_dir)

from flask_ecp.core.encryption import Encryption

# flask app
app = Flask(__name__)


class Config(object):
    DEBUG = True
    TESTING = True
    FLASK_ECP = dict(
        cipher_key='tajny klucz AES'
    )


class TestEncryption(unittest.TestCase):

    @classmethod
    def setUpClass(cls):
        pass

    @classmethod
    def tearDownClass(cls):
        pass

    def setUp(self):
        self.enc = Encryption('access_key', 'iv_key')

    def tearDown(self):
        pass

    def test_init(self):
        self.assertTrue(isinstance(self.enc, Encryption))
        self.assertIsNotNone(self.enc.cipher_hash_key)

    def test_hash_key(self):
        hash_key = self.enc.hash_key('some secret password')
        self.assertIsNotNone(hash_key)
        self.assertTrue(len(hash_key) == 32)

    def test_create_cipher(self):
        cipher_key = self.enc.hash_key('some secret password')
        iv = get_random_bytes(16)
        cipher = self.enc.create_cipher(cipher_key, iv)
        self.assertIsNotNone(cipher)
        self.assertTrue(isinstance(cipher, AES.AESCipher))

    def test_encrypt_with_keys(self):
        iv_key = self.enc.hash_key('some iv', 16)
        cipher_key = self.enc.hash_key('some access', 32)

        status, encrypt_data = self.enc.encrypt('some data to encrypt', cipher_key, iv_key)
        self.assertTrue(status)
        self.assertTrue(isinstance(encrypt_data, dict))

    def test_encrypt_with_no_keys(self):
        status, encrypt_data = self.enc.encrypt('some data to encrypt')
        self.assertTrue(status)
        self.assertTrue(isinstance(encrypt_data, dict))

    def test_decrypt_with_keys(self):
        secret_data = 'some secret data'
        iv_key = self.enc.hash_key('some iv', 16)
        cipher_key = self.enc.hash_key('some access', 32)

        status_enc, encrypted_data = self.enc.encrypt(secret_data, cipher_key, iv_key)

        self.assertTrue(status_enc)
        self.assertTrue(isinstance(encrypted_data, dict))

        status_dec, decrypted_data = self.enc.decrypt(encrypted_data['encrypted_data'], cipher_key, iv_key)
        self.assertTrue(status_dec)
        self.assertTrue(secret_data == decrypted_data)

    def test_decrypt_with_no_key(self):
        secret_data = 'some secret data'

        status_enc, encrypted_data = self.enc.encrypt(secret_data)

        self.assertTrue(status_enc)
        self.assertTrue(isinstance(encrypted_data, dict))

        status_dec, decrypted_data = self.enc.decrypt(encrypted_data['encrypted_data'])
        self.assertTrue(status_dec)
        self.assertTrue(secret_data == decrypted_data)

if __name__ == '__main__':
    unittest.main()
