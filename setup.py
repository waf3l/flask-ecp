# -*- coding: utf-8 -*-
"""
Flask-ECP
------------

Flask extension for encrypted communication protocol.
"""
from setuptools import setup, find_packages


setup(
    name='Flask-ECP',
    version='1.1.4',
    url='',
    license='MIT',
    author='Marcin Wojtysiak',
    author_email='wojtysiak.marcin@gmail.com',
    description='Flask encrypted communication protocol.',
    long_description=__doc__,
    packages=find_packages(),
    zip_safe=False,
    include_package_data=True,
    platforms='any',
    install_requires=[
            'flask',
            'requests',
            'pycrypto',
            'redis',
            'simple-crypt'
        ],
    tests_require=[
            'coverage',
            'mock'
        ],
    test_suite='tests.suite',
    classifiers=[
        'Environment :: Web Environment',
        'Intended Audience :: Developers',
        'License :: OSI Approved :: MIT License',
        'Operating System :: OS Independent',
        'Programming Language :: Python',
        'Topic :: Internet :: WWW/HTTP :: Dynamic Content',
        'Topic :: Software Development :: Libraries :: Python Modules'
    ]
)
